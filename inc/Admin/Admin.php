<?php

/**
 * @package Stepper
 */

namespace Inc\Admin;

class Admin
{

    // Create Custom Rest Endpoint
    public function create_rest_endpoint()
    {
        register_rest_route('v1/stepper', 'submit', array(
            'methods'    => 'POST',
            'callback' => [$this, 'send_stepper_data']
        ));
    }

    public function send_stepper_data($data)
    {
        $params = $data->get_params();

        if (
            empty($params['area_of_work']) || empty($params['contractcountry']) || empty($params['contractname']) || empty($params['date'])
            || empty($params['dd']) || empty($params['email']) || empty($params['fname'])
            || empty($params['jobtitle']) || empty($params['lname']) || empty($params['nn'])
            || empty($params['phone']) || empty($params['pword']) || empty($params['state'])
            || empty($params['team']) || empty($params['uname']) || empty($params['yyyy'])
        ) {
            return new \WP_Error('error', 'All fields are required', array('status' => 400));
        }

        if (!wp_verify_nonce($params['_wpnonce'], 'wp_rest')) {
            return new WP_Rest_Response('Data not Sent', 422);
        }
        unset($params['_wpnonce']);
        unset($params['_wp_http_referer']);

        // Sanitaization
        $fname = sanitize_text_field($params['fname']);
        $lname = sanitize_text_field($params['lname']);

        $postArr = [
            'post_title' =>  $fname.' '. $lname,
            'post_status' => 'publish',
            'post_type' => 'submission',
        ];

        $post_id = wp_insert_post($postArr);

        foreach($params as $label=> $value){
            switch ($label) {
                case 'area_of_work':
                    $value = sanitize_textarea_field($value);
                    break;
                case 'email':
                    $value = sanitize_email($value);
                    break;
                default:
                    $value = sanitize_text_field($value);
            }

            // Update the data and insert
            update_post_meta($post_id, $label, $value);
        }

        $confirmation_message = 'The message was sent successfully!!';
        return new \WP_Rest_Response($confirmation_message, 200);
    }

    // Create submissions page
    public function create_submissions_page()
    {
        $args = [
            'public' => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-list-view',
            'labels' => [
                'name' => 'Submissions',
                'singular_name' => 'Submission'
            ],
            'supports' => false,
            'capability_type' => 'post',
            'capabilities' => [
                'create_posts' => false
            ],
            'map_meta_cap' => true,
            'menu_position' => 30
        ];

        register_post_type('submission', $args);
    }

    // Create Metabox
    public function create_metabox()
    {
    }

    // Custom Submission Columns
    public function custom_submission_columns()
    {
    }

    // Fill Submission Columns
    public function fill_submission_columns()
    {
    }

    // Setup Serach Filter
    public function setup_search_filter()
    {
    }
}
