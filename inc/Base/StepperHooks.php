<?php

/**
 * @package Stepper
 */

namespace Inc\Base;

use \Inc\Base\BaseController;
use \Inc\Templates\StepperForm;
use \Inc\Admin\Admin;

class StepperHooks extends BaseController
{

    public static function register()
    {
        $stepper_form = new StepperForm();

        $admin = new Admin();

        add_shortcode('stepper', array($stepper_form, 'Form'));

        // Rest API
        add_action('rest_api_init', array($admin, 'create_rest_endpoint'));

        // Create Custom post type
        add_action('init', [$admin, 'create_submissions_page']);
    }
}
