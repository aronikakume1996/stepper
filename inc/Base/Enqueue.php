<?php

/**
 * @package Stepper
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
    public function register()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_metro_ui'));
    }

    function enqueue()
    {
        wp_enqueue_style('stepper-style', $this->plugin_url . 'assets/css/stepper.css');
        wp_enqueue_script('stepper-script', $this->plugin_url . 'assets/js/stepper.js');
        wp_enqueue_script( 'jquery-script', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' );
    }

    function enqueue_metro_ui(){
        wp_enqueue_style('metroui-style', 'https://cdn.korzh.com/metroui/v4.5.1/css/metro-all.min.css');
        wp_enqueue_script( 'metroui-script', 'https://cdn.korzh.com/metroui/v4.5.1/js/metro.min.js', [], '1.0.0', true );
    }
}
