<?php

/**
 * @package Stepper
 */

namespace Inc\Base;

use \Inc\Base\StepperHooks;

class Init
{

    public function register()
    {
        StepperHooks::register();
    }
}
