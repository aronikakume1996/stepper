<?php

/**
 * @package Stepper
 */

namespace Inc\Templates;

if (!defined('ABSPATH')) {
    die('You are not allowed to directly access this file.');
}

use Carbon_Fields\Field;
use Carbon_Fields\Container;

class Options
{
    public function register()
    {
        add_action('after_setup_theme', array($this, 'load_carbon_fields'));
        add_action('carbon_fields_register_fields', array($this, 'create_options_page'));
    }
    function load_carbon_fields()
    {
        \Carbon_Fields\Carbon_Fields::boot();
    }

    function create_options_page()
    {
        Container::make('theme_options', __('Stepper Setting'))
            ->set_page_menu_position(30)
            ->set_icon('dashicons-share-alt')
            ->add_fields(array(

                Field::make('checkbox', 'contact_plugin_active', __('Active'))
                    ->set_option_value('yes'),

                Field::make('text', 'contact_plugin_recipients', __('Recipient Email'))
                    ->set_attribute('type', 'email')
                    ->set_attribute('placeholder', 'Enter email'),

                Field::make('textarea', 'contact_plugin_message', __('Confirmation Message'))
                    ->set_attributes(array(
                        'placeholder' => 'Enter message',
                    )),
            ));
    }
}
