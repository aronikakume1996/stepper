<?php

/**
 * @package Stepper
 */

namespace Inc\Templates;

class StepperForm
{
    public function Form()
    { ?>
        <div data-role="stepper" data-steps="5" id="stepper"></div>

        <form id="enquery_form">
            <!-- One "tab" for each step in the form: -->
            <?php
            wp_nonce_field('wp_rest');
            ?>
            <div class="tab">
                <div class="form-header">

                    <h2>General Information</h2>
                    <p>Let's fill out the basics information of the contract</p>
                </div>

                Team:
                <p><input placeholder="Team ..." oninput="this.className = ''" name="team"></p>
                Contract Name
                <p><input placeholder="Contract name..." oninput="this.className = ''" name="contractname"></p>
                <div style="display: flex; 
                justify-content: space-between;
                gap: 5px">
                    <div>
                        Contractor's Country
                        <p>
                            <input placeholder="Contractor country..." oninput="this.className = ''" name="contractcountry">

                        </p>
                    </div>
                    <div>
                        State
                        <p>
                            <input placeholder="State..." oninput="this.className = ''" name="state">

                        </p>
                    </div>

                </div>

                Job Title
                <p><input placeholder="Job title..." oninput="this.className = ''" name="jobtitle"></p>
                Area of Work
                <p>
                    <textarea placeholder="Area of work..." name="area_of_work" id="area_of_work" cols="30" rows="5"></textarea>
                </p>

                Start Date
                <p><input type="date" placeholder="Date ..." oninput="this.className = ''" name="date"></p>


            </div>
            <div class="tab">Name:
                <p><input placeholder="First name..." oninput="this.className = ''" name="fname"></p>
                <p><input placeholder="Last name..." oninput="this.className = ''" name="lname"></p>
            </div>
            <div class="tab">Contact Info:
                <p><input placeholder="E-mail..." oninput="this.className = ''" name="email"></p>
                <p><input placeholder="Phone..." oninput="this.className = ''" name="phone"></p>
            </div>
            <div class="tab">Birthday:
                <p><input placeholder="dd" oninput="this.className = ''" name="dd"></p>
                <p><input placeholder="mm" oninput="this.className = ''" name="nn"></p>
                <p><input placeholder="yyyy" oninput="this.className = ''" name="yyyy"></p>
            </div>
            <div class="tab">Login Info:
                <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
                <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>
            </div>

            <div class="confirmation" id="confirmation" style="display: none;">
                <p>
                    Thank you for submiting the form
                </p>
            </div>
            <div class="btn-container" style="overflow:auto;">
                <div style="display: flex; gap: 5px;float:center;">
                    <button type="button" id="prevBtn" onclick="nextPrev(-1, 'prev')">Previous</button>
                    <button type="button" id="nextBtn" onclick="nextPrev(1, 'next')">Next</button>

                    <button type="submit" id="submit">Submit</button>
                </div>
            </div>
        </form>

        <script>
            var currentTab = 0; // Current tab is set to be the first tab (0)
            showTab(currentTab); // Display the current tab
            let confirmation = document.getElementById("confirmation");
            confirmation.style.display = "none";
            // console.log(confirmation);
            function showTab(n) {
                // This function will display the specified tab of the form...
                var x = document.getElementsByClassName("tab");
                x[n].style.display = "block";
                //... and fix the Previous/Next buttons:
                if (n == 0) {
                    document.getElementById("prevBtn").style.display = "none";
                    document.getElementById("submit").style.display = "none";
                } else {
                    document.getElementById("prevBtn").style.display = "inline";
                }

                if (n == (x.length - 1)) {
                    document.getElementById("nextBtn").innerHTML = "Submit";
                    // document.getElementById("nextBtn").setAttribute("type", "submit");
                    document.getElementById("nextBtn").setAttribute("class", "submit");
                } else {
                    document.getElementById("nextBtn").innerHTML = "Next";
                }
                console.log(n);
                console.log(x.length);
                if (n == x.length) {
                    // document.getElementById("nextBtn").setAttribute("type", "submit");
                    console.log('Final');
                }
                //... and run a function that will display the correct step indicator:
                fixStepIndicator(n)
            }

            function nextPrev(n, m) {

                // This function will figure out which tab to display
                var x = document.getElementsByClassName("tab");
                // Exit the function if any field in the current tab is invalid:
                if (n == 1 && !validateForm()) return false;
                // Hide the current tab:

                // if (n == (x.length - 1)) {
                //     confirmation.style.display = "block";
                // }
                x[currentTab].style.display = "none";
                // Increase or decrease the current tab by 1:
                currentTab = currentTab + n;
                console.log('Current Tab: ', currentTab);

                if (currentTab < 4) {
                    console.log('Less: ', currentTab);
                    document.getElementById("submit").style.display = "none";
                } else {
                    document.getElementById("submit").style.display = "inline";
                    document.getElementById("nextBtn").style.display = "none";

                }
                // if you have reached the end of the form...
                if (currentTab >= x.length) {
                    // ... the form gets submitted:
                    console.log('Current Length: ', x.length);
                    confirmation.style.display = "block";
                    confirmation.setAttribute("style", "display: block");
                    // document.getElementById("regForm").submit();
                    // return false;
                }
                // Otherwise, display the correct tab:
                showTab(currentTab);

                var stepper = Metro.getPlugin('#stepper', 'stepper');
                stepper[m]();
            }

            function validateForm() {
                // This function deals with validation of the form fields
                var x, y, i, valid = true;
                x = document.getElementsByClassName("tab");
                y = x[currentTab].getElementsByTagName("input");
                // y = x[currentTab].getElementsByTagName("textarea");
                // A loop that checks every input field in the current tab:
                for (i = 0; i < y.length; i++) {
                    // If a field is empty...
                    if (y[i].value == "") {
                        // add an "invalid" class to the field:
                        y[i].className += " invalid";
                        // and set the current valid status to false
                        valid = false;
                    }
                }
                // If the valid status is true, mark the step as finished and valid:
                if (valid) {
                    document.getElementsByClassName("step")[currentTab].className += " finish";
                }
                return valid; // return the valid status
            }

            function fixStepIndicator(n) {
                // This function removes the "active" class of all steps...
                var i, x = document.getElementsByClassName("step");
                for (i = 0; i < x.length; i++) {
                    x[i].className = x[i].className.replace(" active", "");
                }
                //... and adds the "active" class on the current step:
                x[n].className += " active";
            }
        </script>
        <script>
            jQuery(document).ready(function($) {
                $('#enquery_form').submit(function(event) {
                    event.preventDefault();
                    let form = $(this);

                    $.ajax({
                        type: 'POST',
                        url: "<?php echo get_rest_url(null, 'v1/stepper/submit'); ?>",
                        data: form.serialize(),

                        success: function(res) {
                            console.log('Success....');
                        },
                        error: function(err) {
                            console.log("Error ....", err);
                        }
                    })
                });

            })
        </script>

        <script>
            function stepperMethod(m) {
                var stepper = Metro.getPlugin('#stepper', 'stepper');
                stepper[m]();
            }
        </script>
<?php
    }
}
